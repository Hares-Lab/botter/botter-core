class BotError(Exception):
    """
    A parent class for all exceptions in this package.
    """
    
    pass

__all__ = \
[
    'BotError',
]
